import { TestBed, async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { ThankYouGuard } from './thankyou.guard';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Registration } from '../../models/registration/registration.model';

import { BehaviorSubject } from 'rxjs';

const defaultRegistration: Registration = {
  title: 'Mr',
  firstName: 'Test',
  lastName: 'Test',
  email: 'test@test.com',
  occupation: 'Tester',
  country: 'Ireland',
  postalCode: 'DY18YE38'
};

class RegistrationServiceStub {
  registration: BehaviorSubject<Registration> = new BehaviorSubject(undefined);
}

class RouterStub {
  navigate(url: any) {
    return url;
  }
}

describe('ThankYouGuard Tests', () => {
  let thankYouGuard: ThankYouGuard;
  let registrationService: RegistrationService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ThankYouGuard,
        { provide: RegistrationService, useClass: RegistrationServiceStub },
        { provide: Router, useClass: RouterStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    thankYouGuard = TestBed.get(ThankYouGuard);
    registrationService = TestBed.get(RegistrationService);
  });

  it('Should not be able to access the thank you screen', () => {
    expect(thankYouGuard.canActivate(null, null)).toBe(false);
  });

  it('Should not be able to access the thank you screen childs', () => {
    expect(thankYouGuard.canActivateChild(null, null)).toBe(false);
  });

  it('Should be able to access the thank you screen', () => {
    registrationService.registration.next(defaultRegistration);
    expect(thankYouGuard.canActivate(null, null)).toBe(true);
  });


  it('Should be able to access the thank you screen childs', () => {
    registrationService.registration.next(defaultRegistration);
    expect(thankYouGuard.canActivateChild(null, null)).toBe(true);
  });

});
