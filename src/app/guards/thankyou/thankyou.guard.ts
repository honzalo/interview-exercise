import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { RegistrationService } from '../../services/registration/registration.service';

@Injectable({
  providedIn: 'root',
})
export class ThankYouGuard implements CanActivate, CanActivateChild {

  constructor(private registrationService: RegistrationService, private router: Router) { }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.registrationService.registration.value !== undefined) { return true; }

    this.router.navigate(['/enter']);
    return false;
  }
}
