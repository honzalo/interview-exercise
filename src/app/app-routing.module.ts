import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThankYouGuard } from './guards/thankyou/thankyou.guard';

const routes: Routes = [
  {
    path: 'enter',
    loadChildren: './pages/enter/enter.module#EnterModule'
  },
  {
    path: 'thankyou',
    loadChildren: './pages/thankyou/thankyou.module#ThankYouModule',
    canActivate: [ThankYouGuard]
  },
  {
    path: '',
    redirectTo: 'enter',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'enter'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
