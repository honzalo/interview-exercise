import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';

import { ThankYouRoutingModule } from './thankyou-routing.module';
import { ThankYouComponent } from './thankyou.component';

@NgModule({
  declarations: [ThankYouComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    ThankYouRoutingModule
  ]
})
export class ThankYouModule { }
