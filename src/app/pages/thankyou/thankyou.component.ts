import { Component, ChangeDetectionStrategy } from '@angular/core';

import { RegistrationService } from '../../services/registration/registration.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThankYouComponent {

  constructor(public registrationService: RegistrationService) { }

}
