import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { ThankYouComponent } from './thankyou.component';

import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Registration } from '../../models/registration/registration.model';

import { BehaviorSubject } from 'rxjs';

class RegistrationServiceStub {
  registration: BehaviorSubject<Registration> = new BehaviorSubject(undefined);
}

class RouterStub {
  navigate(url: any) {
    return url;
  }
}

describe('ThankYouComponent', () => {
  let component: ThankYouComponent;
  let fixture: ComponentFixture<ThankYouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThankYouComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(ThankYouComponent, {
        set: {
          providers: [
            { provide: Router, useClass: RouterStub },
            { provide: RegistrationService, useClass: RegistrationServiceStub },
          ]
        }
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ThankYouComponent);
        component = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
