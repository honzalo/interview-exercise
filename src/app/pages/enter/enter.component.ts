import { Component, OnInit, ChangeDetectionStrategy, DoCheck, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSelectChange } from '@angular/material/select';

import { RegistrationService } from '../../services/registration/registration.service';

import { Observable, fromEvent, from } from 'rxjs';
import { debounceTime, switchMap, map, catchError, pluck } from 'rxjs/operators';

@Component({
  selector: 'app-enter',
  templateUrl: './enter.component.html',
  styleUrls: ['./enter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnterComponent implements OnInit, DoCheck {

  @ViewChild('occupationInput')
  occupationInput: ElementRef;

  @ViewChild('irishPostalCode')
  irishPostalCodeInput: ElementRef;

  @ViewChild('ukPostalCode')
  ukPostalCodeInput: ElementRef;

  form: FormGroup;

  occupationSuggestion$: Observable<any>;

  constructor(private registrationService: RegistrationService, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      firstName: new FormControl('', [Validators.required, Validators.pattern('^([^0-9]*)$')]),
      lastName: new FormControl({ value: '', disabled: true }, [Validators.required, Validators.pattern('^([^0-9]*)$')]),
      email: new FormControl('', Validators.email),
      occupation: new FormControl(''),
      country: new FormControl('', Validators.required),
      postalCode: new FormControl('')
    });

    this.occupationSuggestion$ = fromEvent(this.occupationInput.nativeElement, 'keydown')
      .pipe(
        debounceTime(300),
        pluck('target', 'value'),
        switchMap((value: string) => this.registrationService.getOccupations(value).pipe(
          catchError(() => []),
          map((suggestions: any[]) => suggestions ? suggestions.slice(0, 5) : [])
        )));
  }

  ngDoCheck() {
    this.firstName && this.firstName.valid ? this.lastName.enable() : this.lastName.disable();
  }

  get title(): AbstractControl {
    return this.form.get('title');
  }

  get firstName(): AbstractControl {
    return this.form.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.form.get('lastName');
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }

  get occupation(): AbstractControl {
    return this.form.get('occupation');
  }

  get country(): AbstractControl {
    return this.form.get('country');
  }

  get postalCode(): AbstractControl {
    return this.form.get('postalCode');
  }

  clearOccupations(): void {
    this.occupationSuggestion$ = from([]);
  }

  countrySelectionChanged(newCountrySelection: MatSelectChange): void {
    this.postalCode.setValue('');

    if (newCountrySelection && newCountrySelection.value === 'Ireland') {
      this.postalCode.setValidators([Validators.minLength(6), Validators.maxLength(10)]);
      this.form.updateValueAndValidity();
      setTimeout(() => this.focusElement(this.irishPostalCodeInput), 0);
    }

    if (newCountrySelection && newCountrySelection.value === 'United Kindom') {
      this.postalCode.setValidators([
        Validators.required,
        Validators.pattern('^[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$')
      ]);
      this.form.updateValueAndValidity();
      setTimeout(() => this.focusElement(this.ukPostalCodeInput), 0);
    }
  }

  focusElement(element: ElementRef): void {
    if (element && element.nativeElement) {
      element.nativeElement.focus();
    }
  }

  submitForm(): void {
    if (this.form.valid) {
      this.registrationService.registerData(this.form.value);
      this.router.navigate(['/thankyou']);
    }
  }
}
