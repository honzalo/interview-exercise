import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatInputModule, MatSelectModule, MatAutocompleteModule, MatFormFieldModule } from '@angular/material';

import { EnterComponent } from './enter.component';

import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Registration } from '../../models/registration/registration.model';

import { BehaviorSubject } from 'rxjs';

class RegistrationServiceStub {
  registration: BehaviorSubject<Registration> = new BehaviorSubject(undefined);
}

class RouterStub {
  navigate(url: any) {
    return url;
  }
}

const validRegistration: Registration = {
  title: 'Mr',
  firstName: 'Test',
  lastName: 'Test',
  email: 'test@test.com',
  occupation: 'Tester',
  country: 'Ireland',
  postalCode: ''
};

describe('EnterComponent', () => {
  let component: EnterComponent;
  let fixture: ComponentFixture<EnterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EnterComponent],
      imports: [
        NoopAnimationsModule,
        MatInputModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatFormFieldModule
      ],
      providers: [
        FormBuilder
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(EnterComponent, {
        set: {
          providers: [
            { provide: Router, useClass: RouterStub },
            { provide: RegistrationService, useClass: RegistrationServiceStub },
          ]
        }
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EnterComponent);
        component = fixture.componentInstance;

        component.ngOnInit();
      });
  }));

  function updateForm(registration: Registration) {
    component.title.setValue(registration.title);
    component.firstName.setValue(registration.firstName);
    component.lastName.setValue(registration.lastName);
    component.email.setValue(registration.email);
    component.occupation.setValue(registration.occupation);
    component.country.setValue(registration.country);
    component.postalCode.setValue(registration.postalCode);
  }

  it('should have default props', fakeAsync(() => {
    expect(component.title.value).toEqual('');
    expect(component.firstName.value).toEqual('');
    expect(component.lastName.value).toEqual('');
    expect(component.email.value).toEqual('');
    expect(component.occupation.value).toEqual('');
    expect(component.country.value).toEqual('');
    expect(component.postalCode.value).toEqual('');
  }));

  it('should have an invalid form by start', fakeAsync(() => {
    expect(component.form.valid).toBeFalsy();
  }));

  describe('should validate the form correctly', () => {
    beforeEach(() => {
      updateForm(validRegistration);
    });

    it('should process a valid form', fakeAsync(() => {
      expect(component.form.valid).toBeTruthy();
    }));

    it('should require a title', fakeAsync(() => {
      component.title.setValue('');
      expect(component.form.valid).toBeFalsy();
    }));

    it('should require a first Name', fakeAsync(() => {
      component.firstName.setValue('');
      expect(component.form.valid).toBeFalsy();
    }));

    it('should disable last name if no first name', fakeAsync(() => {
      component.firstName.setValue('');
      component.ngDoCheck();
      expect(component.lastName.disabled).toBeTruthy();
    }));

    it('should require a last Name', fakeAsync(() => {
      component.ngDoCheck();
      expect(component.lastName.disabled).toBeFalsy();
      component.lastName.setValue('');
      expect(component.form.valid).toBeFalsy();
    }));

    it('should not require an email', fakeAsync(() => {
      component.email.setValue('');
      expect(component.form.valid).toBeTruthy();
    }));

    it('should not require an occupation', fakeAsync(() => {
      component.occupation.setValue('');
      expect(component.form.valid).toBeTruthy();
    }));

    it('should require a country', fakeAsync(() => {
      component.country.setValue('');
      expect(component.form.valid).toBeFalsy();
    }));

    it('should require a postalCode if in UK', fakeAsync(() => {
      component.country.setValue('United Kindom');
      component.countrySelectionChanged({ value: 'United Kindom', source: null });
      tick();
      component.postalCode.setValue('');
      expect(component.form.valid).toBeFalsy();
      component.postalCode.setValue('SW1A 1AA');
      expect(component.form.valid).toBeTruthy();
      component.postalCode.setValue('cxz789dasjasd');
      expect(component.form.valid).toBeFalsy();
    }));

    it('should have a valid postalCode if in Ireland', fakeAsync(() => {
      component.country.setValue('Ireland');
      component.countrySelectionChanged({ value: 'Ireland', source: null });
      tick();
      component.postalCode.setValue('');
      expect(component.form.valid).toBeTruthy();
      component.postalCode.setValue('123');
      expect(component.form.valid).toBeFalsy();
      component.postalCode.setValue('D18YE28');
      expect(component.form.valid).toBeTruthy();
      component.postalCode.setValue('D18YE28dasdsad');
      expect(component.form.valid).toBeFalsy();
    }));
  });
});
