import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

import { Registration } from '../../models/registration/registration.model';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  registration: BehaviorSubject<Registration> = new BehaviorSubject(undefined);

  constructor(private http: HttpClient) { }

  getOccupations(occupation: string): any {
    return this.http.get(`${environment.endpoints.occupations}?begins_with=${occupation}`);
  }

  registerData(newRegistration: Registration): void {
    this.registration.next(newRegistration);
  }
}
