import { TestBed, fakeAsync } from '@angular/core/testing';

import { RegistrationService } from './registration.service';
import { Registration } from '../../models/registration/registration.model';

import { defer } from 'rxjs';

const defaultRegistration: Registration = {
  title: 'Mr',
  firstName: 'Test',
  lastName: 'Test',
  email: 'test@test.com',
  occupation: 'Tester',
  country: 'Ireland',
  postalCode: 'DY18YE38'
};

const occupationSearchResult: any = [
  {
    uuid: '1872f252c48cd1725829ddceec7b1030',
    suggestion: 'Tester Operator Helper',
    normalized_job_title: 'tester operator helper',
    parent_uuid: 'e3f3244ad58f52589c7f42d636884892'
  }
];

function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('RegistrationService', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let registrationService: RegistrationService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    registrationService = new RegistrationService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(registrationService).toBeTruthy();
  });

  it('should register a new value', () => {
    expect(registrationService.registration.value).toBeUndefined();
    registrationService.registerData(defaultRegistration);
    expect(registrationService.registration.value.firstName).toBe('Test');
  });

  it('should fetch occupations', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(asyncData(occupationSearchResult));
    registrationService.getOccupations('Tester').subscribe(occupations => {
      expect(occupations[0].suggestion).toBe('Tester Operator Helper');
    });
  }));
});
