export interface Registration {
  title: string;
  firstName: string;
  lastName: string;
  email: string;
  occupation: string;
  country: string;
  postalCode: string;
}
