import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  displayLoading = true;

  onContentLoad(): void {
    this.displayLoading = false;
  }
}
