export const environment = {
  production: true,
  endpoints: {
    occupations: 'https://api.dataatwork.org/v1/jobs/autocomplete'
  }
};
