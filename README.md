# RoomexInterview

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Notes

### Unit test
I haven't been able to complete 100% coverage unit test. But to the point of writing the unit test are on:

```
================= Coverage summary =================
Statements   : 90.59% ( 77/85 )
Branches     : 72.73% ( 16/22 )
Functions    : 84.38% ( 27/32 )
Lines        : 92.59% ( 75/81 )
====================================================
```

### Unrequested features

The /thankyou endpoint is guarded and redirects you to the /enter endpoint if there hasn't been any registration completed.

### Considerations

* The code does not store any information on the browser. If the browser is refreshed any prior update is lost.


## Introduction
Roomex Ltd are creating a Single Page Application

Please note the following:
• Although the requirements are starting small, this will be part of an Enterprise Level application and
consideration must be given to how to scale effectively
• The Application must be written using the Angular framework version 2 or greater.
• It is recommended, but not necessary, to use the Angular CLI to bootstrap your application.
• You are free to use any third-party libraries you see fit.
• UI / UX design is totally within your control. Use CSS as you see fit.
• One of the components or services written for this exercise must be unit tested


## Specifications
The application will initially consist of two routes /enter and /thankyou

### /enter
On this page the company require a form with the following fields and properties.

| Field    |      Description      |  Comments |
|----------|:---------------------:|----------:|
| Title |  A dropdown that is either "Mr.", "Mrs.", "Ms." or "Dr." | Required |
| Firstname |    A text input field   |   Required and cannot contain any numbers |
| Lastname | A text input field |    Required, cannot contain any numbers, disabled until the Firstname input contains a value. |
 | Email |    A text input field   |   Not required, but must be a valid email address if entered | 
 | Occupation | An input that is blank or one of the jobs from an API endpoint | Not required but would like that the user can begin typing an occupation and suggestions are made from http://api.dataatwork.org/v1/jobs/autocomplete?begins_with="<user_input>" | 
 | Country | A dropdown that is either "Ireland" or "United Kingdom" | Required | 
 | Post Code | A text input field | • If "Country" is "Ireland" this is not required, but must be 6-10 characters long if entered • If "Country" is "United Kingdom" text is required and must conform to the regex "^[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$" | 


Additionally, the company requests the following:
• The Submit button is never disabled, if the user clicks submit and the form is invalid, validation
messages should appear under each problem field with a message to explain the problem.
• Please notify the user of any errors in their input as they move through the form.
If the form is valid, the user is brought to the /thankyou page

### /thankyou
On this page, please display the all the data entered on the previous page in a human readable
format e.g.
Name: Mr. Shane Healy
Occupation: Software Developer
Etc...

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

